from jenkins
user root
RUN apt-get update && apt-get upgrade -y && \
	apt-get -y install git gcc-arm-none-eabi build-essential cmake
user jenkins
